Dotfiles
=======

Cloudstation  dotfiles

# Installation

    clone the project in ~/.config/ directory
    $ git clone https://gitlab.com/mobilefirstcentury/dotfiles.git ~/.config/dotfiles

    create symlinks for software unaware of config directory 
    $ source ~/.config/dotfiles/install.sh


