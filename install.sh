#! /usr/bin/env bash

# MFC_SCRIPT_MAIN is used to encapsulate this script to not pollute global scope 
# function is defined in '()' instead of '{}' to create a subshell enforcing the encapsulation
function MFC_SCRIPT_MAIN() (
    err() {
        echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
    }

    saferm() {
        if [ -f "$1"  ]; then
            rm --preserve-root "$1"
	fi	
        if [ -d "$1"  ]; then
            rm --preserve-root "$1"
	fi	
    }
    
    # backups argument file or directory in "$BACKUP_DIR"
    # will return an error code if the 
    # that way 'backup file && rm file" work as intended
    backup () {
        if [ -f "$1" ] || [ -d "$1" ]; then
	    cp -r "$1" "$BACKUP_DIR/$1"
            echo "created a backup of $1"
	    return 0
	else
	    return 1
	fi
    }

    # symlinks XDG compliant config dir to config dir in dotfiles project 
    # arguments:
    #     $0: XDG config dir                   (example: git
    #     $1: XDG config file                  (example: .gitconfig)
    # example 'symconf tmux tmux.conf' would symlink ~/.config/tmux/tmux.conf 
    symconf() {
	if [ -z "$0" ] || [ -z "$1" ]; then
            err "Illegal empty arguments in symconf function"	    
        fi
	xdg_dir=$0
	xdg_file=$1
	home_conf=$2
	[ -z "$home_conf" ] && home_conf=$1

        backup "~/$home_conf" && saferm "~/$home_conf"
        backup "$XDG_CONFIG_HOME/$xdg_dir" && saferm -rf "$XDG_CONFIG_HOME/$xdg_dir"
        mkdir -p $XDG_CONFIG_HOME/$xdg_dir && ln -s "$DIR/$0"  "$XDG_CONFIG_HOME/$xdg_dir" 
    }

    # get the source dir of this script
    # Not an easy deal, got the solution [[https://stackoverflow.com/a/51651602/5785556][here]]
    DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"


    # symlink here all our software confs
    symconf git .gitconfig 
    syconf tmux .tmux.conf
)

# THIS PART MODIFIES SHELL GLOCAL SCOPE

# tmux won't abide by XDG BASE DIRECTORY spec.
# this is a work around sourced [[https://github.com/tmux/tmux/issues/142#issuecomment-329946562][here]]
alias tmux='TERM=xterm-256color tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf'

# THIS PART DOESN'T MODIFY SHELL GLOBAL SCOPE 
MFC_SCRIPT_MAIN


